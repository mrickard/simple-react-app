import React from "react";

const JokeList = props => {

  return (
    <div className="jokeList">
      {props.jokes.map(({Q, A, V, current}, idx) => {

        let cName = '';

        if (current) {
          cName = 'current';
        }

          return (
            <div className={'jl ' + cName} key={'joke-' + idx}>
              {Q}
              <span className='funnyCharacter'>
                {V > 0 && '✔'}
                {V < 0 && 'x'}
              </span>
            </div>
          )
        }

      )}
    </div>
  );
}

export default JokeList;

