import React, { Component } from "react";
import JokeList from "./JokeList";
import CurrentJoke from "./CurrentJoke";

class JokeListContainer extends Component {

  constructor() {
    super();
    this.state = {
      jokes: [
        {current: true, Q: "What kind of bagel can fly?",  A: "A plain bagel", V: 0},
        {current: false, Q: "How is imitation like a plateau?",  A: "They’re both the highest form of flattery", V: 0},
        {current: false, Q: "What's the best thing about living in Switzerland?",  A: "I don’t know, but the flag is a big plus.", V: 0},
        {current: false, Q: "What did the pirate say when he turned 80?",  A: "Aye matey!", V: 0},
        {current: false, Q: "Why can’t bicycles stand up on their own?",  A: "They’re too tired.", V: 0},
        {current: false, Q: "Where do you see yourself in five years?",  A: "Celebrating the five-year anniversary of you asking me this question.", V: 0},
      ]
    }
  }

  /*
    I opted to have the state manipulating click handler in the container
    for separation of data and presentation.  Having data and its manipulation
    in the parent context, though, means that I need to pass context or bind it.
    While I could have bound it, I stuck with passing it explicitly for clarity.
  */

  vote(idx, value, context) {

    context.setState((prevState) => {

      let jokes = [...prevState.jokes];

      jokes[idx].V = jokes[idx].V + value;

      jokes[idx].current = false;

      if (jokes[idx + 1]) {
        jokes[idx + 1].current = true;
      } else {
        jokes[0].current = true;
      }

      return {jokes};
    });

  }

  render() {
    return (
    <div>
      <CurrentJoke jokes={this.state.jokes} vote={this.vote} context={this} />
      <JokeList jokes={this.state.jokes} />
    </div>
    );
  }

}

export default JokeListContainer;

