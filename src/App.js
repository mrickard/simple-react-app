import React, { Component } from 'react';
import JokeListContainer from "./JokeListContainer";
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Vote-a-joke</h1>
        </header>
        <JokeListContainer />
      </div>
    );
  }
}

export default App;
