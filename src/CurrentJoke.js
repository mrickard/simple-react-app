import React, { Component } from "react";

class CurrentJoke extends Component {

  constructor() {
    super();
    this.state = {showPunch: false};

  }

  togglePunch() {

    this.setState((prevState) => {
      return {showPunch: !prevState.showPunch};
    });

  }

  render() {

  return (
    <div>
      {this.props.jokes.map(({Q, A, V, current}, idx) => {

          if (current) {

            return (
            <div className="currentJoke" key={'current-' + idx}>
              <div className="q">{Q}</div>

              {this.state.showPunch &&
                <div>
                  <span className='punchline'>{A}</span>

                  <div className="controls">

                  <button onClick={(e) => {
                      this.props.vote(idx, 1, this.props.context);
                      this.togglePunch();
                    }
                  }>Funny!</button>

                  <button onClick={(e) => {
                      this.props.vote(idx, -1, this.props.context);
                      this.togglePunch();
                    }
                  }>Boo!</button>

                </div>

                </div>
              }

              {! this.state.showPunch &&
                  <button onClick={ () => { this.togglePunch() } }>Show punchline</button> }
            </div>
            )
          }
          return null;

        }

      )}
    </div>
  );

  }
}

export default CurrentJoke;

